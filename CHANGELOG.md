[0.1.0]
* Initial version

[1.0.0]
* Added LDAP support
* Updated logo and information

[1.0.1]
* Updated links to developers

[1.0.2]
* Updated to v2.1.3

[1.0.3]
* Updated to v2.1.5
  *  Fix search from upstream not work (registry.npmjs.org) - (@Meeeeow in #166)
  *  Fix package list leak in search & other bug fixes - (@Meeeeow in #163)
  *  Add docs about run behind reverse proxy - (@Meeeeow in #160)

[1.1.0]
* Updated to v2.1.7
* Custom configuration can be set in `/app/data/config.yml`
* Breaking change - all repositories are set to private access and publish by default
* Run verdaccio as normal user

[1.2.0]
* Updated to v2.2.0
  *  Allow url_prefix to be only the path - (@BartDubois ) in (#197)
  *  don't blindly clobber local dist-tags - (@rmg ) in (#206)
  *  Adds cache option to uplinks - (@silkentrance ) in (#132)

[1.2.1]
* Update to v2.2.1
  * config section moved up, some keywords added - [#211]
  * docs: update docs with behind reverse proxy - [#214]
  * Add remote ip to request log - [#216]

[1.2.2]
* Update to v2.2.3
  * Updated Dockerfile & added proper signal handling - [#239]
  * Improve Docker Build - [#181]
  * Add documentation - [#229]
  * Fix #[73] npm-latest support - [#228]

[1.2.3]
* Update to v2.2.6
  * update node version due security update announcement - [#251]

[1.3.0]
* Update to v2.3.0

[1.3.1]
* Update to v2.3.1

[1.3.2]
* Update to v2.3.5
* configuration files inconsistencies, add unit test (644c098)
* Remove accept header that seems cause issues #285 #289 and npm search fails (fab8391)

[1.3.3]
* Update to v2.3.6
* link was broken (a9481cc)
* Correct accept header set for registry requests (#295)
* Update SSL documentation (#296)
* Fix auth process to check against username also and not just groups (#293)

[1.4.0]
* Update to v2.4.0
* check error code to prevent data loss (5d73dca)
* check error code to prevent data loss (93aae05)
* Package metadata cache not work (4d6a447)
* Fixed bug with Maximum call stack size exceeded on packages web API (#326)
* fix: Package metadata cache does not work (#317)
* Debug log color in terminal is too dark (#311)
* docs: Add new sections to documentation (#308)
* Remove from web section not longer valid properties (#307) (#309)
* Fix possible data loss upstream (#306) (#300)

[1.5.0]
* Update to v2.5.0
* check error code to prevent data loss (5d73dca) fix #329
* Fix #334 UI failure on IE 11, add suppor for old browsers. (f1f15be)

[1.6.0]
* Update to v2.6.0

[1.7.0]
* Update verdaccio to 2.7.0

[1.7.1]
* Update logo

[1.7.2]
* Update logo

[1.7.3]
* Update verdaccio to 2.7.3

[1.7.4]
* Update verdaccio to 2.7.4

[1.8.0]
* Update verdaccio to 3.1.1
* New UI, skin and sidebar
* 100% backward compatible with Sinopia (1.4.0) and Verdaccio (2.x)
* groups are not array were not handled (a62688f)
* improve bundle size (a79d87b)

[1.8.1]
* Update verdaccio to 3.1.2

[1.9.0]
* Update verdaccio to 3.2.0
* allowing to allow replace static enpoint in case of reverse proxy (90803c7)
* fixed linebreaks lint issue (9e3ab09)
* normalize package was broken #758 (3717ad4)
* Add basic package infos and resource links to sidebar. (7bd3a4f)
* add published package support to template (4245636)
* added information about package publisher for notifications (1ca5298)

[1.10.0]
* Update verdaccio to 3.3.0
* add RemoteUser type for auth (2f4dbe8)
* adds webpack banner plugin to tag bundles with version (#784) (dac28d3)
* dynamic date generation for component test (e5ea0c2)
* ignores http_proxy and https_proxy (d04dc8d)
* improves regex for ascii and test (#461) (be3968f)
* license field alignment on web ui (#761) (9fa523a)
* open external tabs in new tabs (25e8e60)
* package.json to reduce vulnerabilities (389e306)
* solve seo issue #760 (62d3033)
* wrong auth plugin signature (5c2c414)
* adds support for ascii-doc preview in readme (#464) (29bb57a)
* adds support for external plugin directory (#532) (11dcf79)
* capitalises logged in username (#752) (0e21e35)

[1.11.0]
* Update verdaccio to 3.4.0
* Authentication Plugins / plugin[method] is not a function
* adds base64 support in webui token

[1.11.1]
* Update verdaccio to 3.4.1

[1.12.0]
* Update verdaccio to 3.5.1

[1.13.0]
* Update verdaccio to 3.6.0

[1.14.0]
* Update verdaccio to 3.7.0
* bugs related to logging with type json (#893) (cd231ba)
* login without reload (#678) (#679) (#914) (9cd3ccb)
* path to static directory could be changed (#942) (5557ce5)
* removes asciidoctor.js support (#884) (#947) (cf05938)

[1.14.1]
* Update verdaccio to 3.7.1
* login modal validation (#958) (9f78c31)
* ui change details issue in props update (#959) (#960) (431e760)

[1.15.0]
* Update verdaccio to 3.8.0
* missing properties for default matcher #981 (#982) (3ca20d0)

[1.15.1]
* Update verdaccio to 3.8.1

[1.15.2]
* Update verdaccio to 3.8.2

[1.16.0]
* Update base image

[1.16.1]
* Update verdaccio to 3.8.5
* don't exit if using https and pfx is enabled
* word wrap longer description

[1.16.2]
* Update verdaccio to 3.8.6

[1.17.0]
* Update verdaccio to 3.9.0
* display package peer deps (#1144) (9b52b1d)

[1.18.0]
* Update verdaccio to 3.10.0
* allows package.json as package name (#1149) (6554973)

[1.18.1]
* Update verdaccio to 3.10.1
* remove useless secureProtocol option

[1.18.2]
* Update verdaccio to 3.10.2

[1.19.0]
* Update verdaccio to 3.11.0

[1.19.1]
* Update verdaccio to 3.11.1

[1.19.2]
* Update verdaccio to 3.11.2

[1.19.3]
* Update verdaccio to 3.11.3

[1.19.4]
* Update verdaccio to 3.11.4

[1.19.5]
* Update verdaccio to 3.11.5

[1.19.6]
* Update verdaccio to 3.11.6

[1.19.7]
* Update verdaccio to 3.11.7
* update dependencies security corcern (398b839)

[1.20.0]
* Update verdaccio to 3.12.0

[1.21.0]
* Update Verdaccio to 4.0.0
* [Release announcement](https://verdaccio.org/blog/2019/05/19/15-verdaccio-4-release)
* New fresh User Interface

[1.21.1]
* Update Verdaccio to 4.0.1

[1.21.2]
* Fixup post installation message

[1.22.0]
* Update Verdaccio to 4.0.3
* Use manifest v2

[1.22.1]
* Update Verdaccio to 4.0.4

[1.23.0]
* Update Verdaccio to 4.1.0
* parse YAML/JSON/JS config file (#1258) (95d134b)
* plugin support to filter packages (b9ffac5), closes #818

[1.24.0]
* Update Verdaccio to 4.2.0

[1.24.1]
* Update Verdaccio to 4.2.1

[1.24.2]
* Update Verdaccio to 4.2.2
* mkdir failed and ec2 infinitely re-launch (342d284)
* update @verdaccio/ui-theme@0.2.3 (#1451)
* use test + mkdir for strict dir check (9006146)

[1.25.0]
* Update Verdaccio to 4.3.0

[1.25.1]
* Update Verdaccio to 4.3.1

[1.25.2]
* Update Verdaccio to 4.3.3

[1.25.3]
* Update Verdaccio to 4.3.4

[1.25.4]
* Update Verdaccio to 4.3.5

[1.26.0]
* Update Verdaccio to 4.4.0
* search api will not set magic date header when return array (#1598) (158de3f)
* [Full changes](https://github.com/verdaccio/verdaccio/releases/tag/v4.4.0)

[1.26.1]
* Update Verdaccio to 4.4.1
* prevent issue with leading hyphen in package name - fixes #1429
* warning due ui dependecy #1638
* [Full changes](https://github.com/verdaccio/verdaccio/blob/32aabca6412afdd19a2603205d661f00a667088f/CHANGELOG.md#441-2020-01-03)

[1.26.2]
* Update Verdaccio to 4.4.2

[1.26.3]
* Update Verdaccio to 4.4.3

[1.26.4]
* Update Verdaccio to 4.4.4

[1.27.0]
* Update Verdaccio to 4.5.1

[1.28.0]
* Use latest base image 2.0.0
* Update Verdaccio to 4.6.1

[1.28.1]
* Update Verdaccio to 4.6.2

[1.29.0]
* Update Verdaccio to 4.7.0

[1.29.1]
* Update Verdaccio to 4.7.1

[1.29.2]
* Update Verdaccio to 4.7.2

[1.30.0]
* Update Verdaccio to 4.8.0
* [Full changelog](https://github.com/verdaccio/verdaccio/blob/master/CHANGELOG.md#480-2020-07-16)
* deps: bump @verdaccio/ui-theme from 1.11.0 to 1.12.0 (#1872) (0348bf6)
* package.json & yarn.lock to reduce vulnerabilities (#1876) (e374a62)

[1.30.1]
* Update Verdaccio to 4.8.1
* [Full changelog](https://github.com/verdaccio/verdaccio/blob/master/CHANGELOG.md#481-2020-08-06)
* package.json & yarn.lock to reduce vulnerabilities (#1879) (7c50d87)
* package.json & yarn.lock to reduce vulnerabilities (#1889) (5e94478)
* update readme library (#1892) (f75ad87)

[1.31.0]
* Update Verdaccio to 4.9.0
* [Full changelog](https://github.com/verdaccio/verdaccio/blob/master/CHANGELOG.md#490-2020-11-22)
* buffer deprecated warnings (#1993) (4feaf24)
* check author if lastest is not found (#1994) (185babc)
* package.json & yarn.lock to reduce vulnerabilities (#1910) (cb5a8a7)
* update dependencies (#1965) (1443b73)
* update dependencies (#1991) (ef51294)
* update dependencies, docker base and build deps (#2007) (6eef015)
* deps: bump @verdaccio/ui-theme from 1.12.1 to 1.13.1 (#1961) (f7aad33)

[1.31.1]
* Update Verdaccio to 4.9.1
* [Full changelog](https://github.com/verdaccio/verdaccio/blob/master/CHANGELOG.md#bug-fixes)

[1.32.0]
* Update Verdaccio to 4.10.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v4.10.0)

[1.33.0]
* Update Verdaccio to 4.11.0
* enable keep alive (#2067) (4d152ca)
* skip log static content from web (#2027) (39376be), closes #2026
* update @verdaccio/ui-theme (#2026) (2ea17ce)
* update UI theme (#2065) (936aa30)
* update dependencies (#2066) (f005b37)

[1.34.0]
* Use latest base image 3.0.0

[1.34.1]
* Update Verdaccio to 4.11.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v4.11.1)

[1.34.2]
* Update Verdaccio to 4.11.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v4.11.2)

[1.34.3]
* Update Verdaccio to 4.11.3
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v4.11.3)

[1.35.0]
* Update Verdaccio to 4.12.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v4.12.0)

[1.36.0]
* Update Verdaccio to 5.0.0
* improve url_prefix behavior (#2122) (15bb350)
* npm token support revisited and enabled by default (#2145) (8cc6393)

[1.36.1]
* Update Verdaccio to 5.0.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.0.1)
* logo and favicon configuration

[1.36.2]
* Update Verdaccio to 5.0.3
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.0.3)

[1.36.3]
* Update Verdaccio to 5.0.4
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.0.4)

[1.37.0]
* Update Verdaccio to 5.1.0
* implement search v1 endpoint (#2256) (251bd95)
* tarball url redirect (#1688) (78d04cf)
* update Node.js docker image 14.17.0 (#2247) (4ed7286)
* restore using local path web logo (#2270) (8434cc5)
* update core dependencies (#2269)

[1.37.1]
* Update Verdaccio to 5.1.1
* add logger fatal if fails on startup (#2288) (558dd3f), closes #2287
* improve get headers from request #2190 (#2271) (38ca095)

[1.37.2]
* Update Verdaccio to 5.1.2
* remove token experiment flag (#2332) (2924054)
* update dependencies (#2291) (b421ed8)

[1.37.3]
* Update Verdaccio to 5.1.3
* plugin allow_publish (pkg) got a undefined version (#2315) (ecfc4c3)

[1.37.4]
* Update Verdaccio to 5.1.4
* npm7 audit fix and bulk endpoint (#2426) (2c59091)

[1.37.5]
* Update Verdaccio to 5.1.5
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.1.5)
* add finish language to ui (#2443) (360bec9)

[1.37.6]
* Update Verdaccio to 5.1.6
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.1.6)

[1.38.0]
* Update Verdaccio to 5.2.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.2.0)

[1.38.1]
* Update Verdaccio to 5.2.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.2.1)

[1.38.2]
* Update Verdaccio to 5.2.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.2.2)

[1.39.0]
* Update Verdaccio to 5.3.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.3.0)

[1.39.1]
* Update Verdaccio to 5.3.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.3.1)

[1.39.2]
* Update Verdaccio to 5.3.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.3.2)
* Update to base image v3.2.0

[1.40.0]
* Update Verdaccio to 5.4.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.4.0)
* add cache-control header to endpoints (#2791) (fd3ad1e)
* add rate limit to user api endpoints (#2800) (#2799) (f64e403)

[1.40.1]
* Update Verdaccio to 5.5.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.5.0)
* Upgrade @verdaccio-ui-theme (d36b8b1)
* Use backward compatible UI from v6 on v5 (#2912) (b79266d)
* Update dependencies, include security flagged ones.

[1.40.2]
* Update Verdaccio to 5.5.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.5.1)
* remove engines restriction from ui-theme dependency (de7713e)
* deps: update dependency marked to v4 (#2956) (5009797)
* deps: update dependency mime to v3 (#2957) (ac4392a)

[1.40.3]
* Update Verdaccio to 5.5.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.5.2)
* re-opening log files using SIGUSR2 (#2966) (b964c0d) by @marvinthepa

[1.41.0]
* Update Verdaccio to 5.6.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.6.0)
* update dependencies

[1.41.1]
* Update Verdaccio to 5.6.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.6.2)
* update dependency @verdaccio/ui-theme to v6.0.0-6-next.18 (#3019) (7cff3f7)
* update dependency pino to v6.14.0 (#3018) (08b72b4)

[1.42.0]
* Update Verdaccio to 5.7.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.7.0)
* deps: update dependency @verdaccio/ui-theme to v6.0.0-6-next.20 (#3028) (87e5e74) by @juanpicado
* feat: config control for colors in logs (https://github.com/verdaccio/verdaccio/pull/3011) by @osher

[1.42.1]
* Update Verdaccio to 5.7.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.7.1)

[1.43.0]
* Update Verdaccio to 5.8.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.8.0)

[1.44.0]
* Update Verdaccio to 5.9.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.9.0)
* deps: update dependency @verdaccio/ui-theme to v6.0.0-6-next.23 (#3106) (b963f7d) by @juanpicado

[1.45.0]
* Update Verdaccio to 5.10.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.10.0)

[1.45.1]
* Update Verdaccio to 5.10.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.10.1)
* duplicated groups on use jwt tokens

[1.45.2]
* Update Verdaccio to 5.10.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.10.2)
* deps: update dependency dayjs to v1.11.2 (#3159) (cd39a42)
* deps: update dependency express to v4.18.1 (#3160) (5fffeac)
* deps: update dependency lru-cache to v7.9.0 (#3161) (f16a1ea)
* remove lru deprecation warning (#3158) (adfbefc)
* duplicated groups on use jwt tokens

[1.45.3]
* Update Verdaccio to 5.10.3
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.10.3)
* deps: update dependency @verdaccio/readme to v10.3.4 (#3197) (2443900)
* deps: update dependency marked to v4.0.16 (#3187) (a17378d)
* deps: update dependency verdaccio-audit to v10.2.2 (#3201) (9e743f4)

[1.46.0]
* Update Verdaccio to 5.11.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.11.0)
* run server init as promise (#3210) (42194c7)

[1.47.0]
* Update Verdaccio to 5.13.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.13.0)
* allow other password hashing algorithms

[1.47.1]
* Update Verdaccio to 5.13.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.13.1)
* There was a regression on initial release https://github.com/verdaccio/verdaccio/issues/2141 where the location of the storage was not taken in account, hopefully does not break anything.

[1.47.2]
* Update Verdaccio to 5.13.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.13.2)

[1.47.3]
* Update Verdaccio to 5.13.3
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.13.3)

[1.48.0]
* Update Verdaccio to 5.14.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.14.0)

[1.49.0]
* Update Verdaccio to 5.15.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.15.1)
* implement abbreviated manifest (#3343) (966139f) by @juanpicado

[1.49.1]
* Update Verdaccio to 5.15.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.15.2)
* get header by quality priority value

[1.49.2]
* Update Verdaccio to 5.15.3
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.15.3)
* remove stringify with space responses

[1.49.3]
* Update Verdaccio to 5.15.4
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.15.4)
* deps: update dependency @verdaccio/ui-theme to v6.0.0-6-next.48 (#3404) (be5c9f9) by @mbtools

[1.50.0]
* Update Verdaccio to 5.16.3
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.16.0)

[1.51.0]
* Update Verdaccio to 5.17.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.17.0)
* Upgrade to React 18 + react dependencies
* highlight readme source code (#3506) (8715a5c)
* sec: update base image to v18.12.1-alpine (#3489) (07144c9)

[1.52.0]
* Update Verdaccio to 5.18.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.18.0)
* export module types (TypeScript types) (#3508) (0a0b772)
* add hasInstallScript calculation (#3509) (0b49566) solves #3366

[1.53.0]
* Update Verdaccio to 5.19.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.19.0)

[1.53.1]
* Update Verdaccio to 5.19.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.19.1)

[1.54.0]
* Update Verdaccio to 5.20.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.20.1)

[1.55.0]
* Update Verdaccio to 5.21.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.21.1)

[1.55.1]
* Update Verdaccio to 5.21.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.21.2)
* avoid setting body for GET requests (#3643) (e4573c7), closes #3601 #3601 by @melodyVoid
* update dependencies (#3649) ([ed80a25] (https://github.com/verdaccio/verdaccio/commit/ed80a25c08bf2971169b8ccfec96e404d019f2b1)) fix #3612 @juanpicado

[1.56.0]
* Update Verdaccio to 5.22.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.22.0)
* improved configuration parsing and token signature (#3658) (e50d4d9)
* the next major will include an enhancedLegacySignature property to be able get rid of [DEP0106] DeprecationWarning: crypto.createDecipher is deprecated. and improve legacy token signature.
* Logger level configuration was being ignored (#3658) (e50d4d9) regresion at v5.20.0

[1.56.1]
* Update Verdaccio to 5.22.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.22.1)
* #3666 experiments config fail on startup (#3668) (f6c22d3) by @juanpicado

[1.57.0]
* Update Verdaccio to 5.23.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.23.0)

[1.57.1]
* Update Verdaccio to 5.23.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.23.1)
* package.json main field (#3700) (5d83c52) kudos to @pieh

[1.57.2]
* Update Verdaccio to 5.23.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.23.2)
* deps: update dependency @verdaccio/config to v6.0.0-6-next.67 (#3716) (8e79e5f)
* ui package search (#3713) (1029d7a) solves https://github.com/verdaccio/verdaccio/issues/3709 by @jzhangdev

[1.58.0]
* Update Verdaccio to 5.24.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.24.1)
* upgrade plugin htpassw major (#3712) (e939ca2)
* deps: update dependency semver to v7.5.0 (#3733) (dcd530b)
* deps: update dependency verdaccio-htpasswd to v10.5.5 (#3749) (b3ea816) (overrided by #3712)
* bug: file count: undefined #3758

[1.59.0]
* Update Verdaccio to 5.25.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.25.0)
* replace internal web search indexer @orama/orama (#3818) (770cd27)
* deps: update core verdaccio dependencies (#3822) (2d6dbc7)
* deps: update dependency @babel/eslint-parser to v7.21.8 (#3807) (271f918)
* deps: update dependency semver to v7.5.1 (#3816) (373c584)

[1.60.0]
* Update Verdaccio to 5.26.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.26.0)
* refactor middleware and bufixes (#3922) (43de79d)
* fix: official package "-" cannot be synced #3915
* deps: update core verdaccio dependencies (#3873) (f0bb451)
* deps: update dependency clipanion to v3.2.1 (#3874) (b39c01f)
* deps: update dependency semver to v7.5.2 (#3895) (a699787)
* deps: update dependency verdaccio-audit to v11.0.0-6-next.35 (#3877) (a84d178)
* remove req.host deprecation warning (#3921) (4be19db)
* update dependencies (971d500)

[1.60.1]
* Update Verdaccio to 5.26.1
* Update Verdaccio LDAP to 6.0.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.26.1)
* deps: update dependency validator to v13.11.0 (#3967) (f61cfda)
* update core depepedencies 5.x (#3991) (528a902), closes [#3988] (https://github.com/verdaccio/verdaccio/issues/3988) #3989 by @moglerdev

[1.60.2]
* Update Verdaccio to 5.26.3
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.26.3)
* fix: ui expired token by @Ku3mi41 https://github.com/verdaccio/verdaccio/pull/4007
* update core dependencies (#4035) (bd0d029)

[1.61.0]
* Update Verdaccio to 5.27.0
* Update base image to 4.2.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.27.0)
* A new indicator was added on the detail page action button and the packages view (small icon to download)
* fix dark mode and readme css support
* fix global for yarn packages and add version to the packages on copy
* feat: hide deprecated versions option
* fix: improve deprecated package style
* Screenshot 2023-10-14 at 16 54 03
* feat: display deprecated versions

[1.61.1]
* Update Verdaccio to 5.27.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.27.1)

[1.62.0]
* Update Verdaccio to 5.28.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.28.0)
* restore search all endpoint

[1.63.0]
* Update Verdaccio to 5.29.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.29.0)
* replace request with @cypress/request (#4234) (e11d95d) by @legobeat

[1.63.1]
* Update Verdaccio to 5.29.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.29.1)

[1.63.2]
* Update Verdaccio to 5.29.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.29.2)
* deps: update core verdaccio dependencies (5.x) (#4516) (11f3309)
* bug: fix: ui dialog break pages on open due remark error #4514 (regression from v5.29.1)

[1.64.0]
* Update Verdaccio to 5.30.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.30.2)
* On the versions view there is a new filter that allows semantic versioning filtering (feedback is welcome)
* update @verdaccio/search-indexer engine restrictions

[1.64.1]
* Update Verdaccio to 5.30.3
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.30.3)

[1.65.0]
* Update Verdaccio to 5.31.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.31.0)
* add property forceMigrateToSecureLegacySignature (#4625) (2941522) #4601
* deps: update dependency express to v4.19.2 (#4596) (4123dbc)

[1.65.1]
* Remove config.yaml merging logic

[1.65.2]
* Update Verdaccio to 5.31.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.31.1)
* deps: update core verdaccio dependencies (#4668) (9e3a79d)
* patch(core/url): Throw if VERDACCIO_FORWARDED_PROTO resolves to an array https://github.com/verdaccio/verdaccio/pull/4613. by @Tobbe
* fix: log spacing depending on the FORMAT and COLORS options #4631 by @somethingSTRANGE
* deps: update node.js to v20.14.0 (#4663) (6591927)
* set engine to 14.21.3 (6cf6064)
* set engine to Node.js 14 (9a6d5b3)

[1.66.0]
* Update Verdaccio to 5.32.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.32.0)

[1.66.1]
* Update Verdaccio to 5.32.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.32.1)

[1.66.2]
* Update Verdaccio to 5.32.2
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v5.32.2)

[1.67.0]
* Update Verdaccio to 6.0.0
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v6.0.0)
* deps: update dependency debug to v4.3.7 (#4838) (cdf7810)
* deps: update dependency envinfo to v7.14.0 (#4843) (c36d87f)

[1.67.1]
* Update Verdaccio to 6.0.1
* [Full changelog](https://github.com/verdaccio/verdaccio/releases/tag/v6.0.1)

[1.67.2]
* Update verdaccio to 6.0.2
* [Full Changelog](https://github.com/verdaccio/verdaccio/releases/tag/v6.0.2)
* fix(deps): update dependency compression to v1.7.5 (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4934
* chore(deps): update yarn to v3.8.6 (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4933
* fix(deps): update core verdaccio dependencies (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4952

[1.67.3]
* Update verdaccio to 6.0.3
* [Full Changelog](https://github.com/verdaccio/verdaccio/releases/tag/v6.0.3)
* fix(deps): update dependency express to v4.21.2 (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4988
* fix(deps): update node.js to v20.18.1 (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4994
* fix(deps): update dependency [@&#8203;cypress/request](https://github.com/cypress/request) to v3.0.7 (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4995
* fix(deps): update dependency debug to v4.4.0 (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4996
* chore(deps): update babel monorepo (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4997
* chore(deps): update dependency nock to v13.5.6 (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/4998
* fix [#&#8203;4984](https://github.com/verdaccio/verdaccio/issues/4984) improved paths for web endpoint by [@&#8203;juanpicado](https://github.com/juanpicado) in https://github.com/verdaccio/verdaccio/pull/5000
* fix(deps): update core verdaccio dependencies (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/5001 [@&#8203;mbtools](https://github.com/mbtools) (more notes will be added)

[1.67.4]
* Update verdaccio to 6.0.4
* [Full Changelog](https://github.com/verdaccio/verdaccio/releases/tag/v6.0.4)
* fix(api): fix scope handling in package routes by [@&#8203;kuoruan](https://github.com/kuoruan) in https://github.com/verdaccio/verdaccio/pull/5006

[1.67.5]
* Update verdaccio to 6.0.5
* [Full Changelog](https://github.com/verdaccio/verdaccio/releases/tag/v6.0.5)
* fix(deps): update core verdaccio dependencies (6.x) by [@&#8203;renovate](https://github.com/renovate) in https://github.com/verdaccio/verdaccio/pull/5014

