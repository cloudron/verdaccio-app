FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg
WORKDIR /app/pkg

# renovate: datasource=npm depName=verdaccio
ARG VERDACCIO_VERSION=6.0.5

# renovate: datasource=npm depName=verdaccio-ldap
ARG LDAP_VERSION=6.0.0

# https://github.com/Alexandre-io/verdaccio-ldap
RUN npm install -g verdaccio@${VERDACCIO_VERSION} verdaccio-ldap@${LDAP_VERSION}

COPY start.sh config.yaml.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
