#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    RegClient = require('npm-registry-client'),
    safe = require('safetydance'),
    timers = require('timers/promises'),
    util = require('util'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

const execAsync = util.promisify(require('node:child_process').exec);

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let app;
    let browser;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const email = 'test@cloudron.io';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function clearCache() {
        await browser.get('https://' + app.fqdn);
        await browser.manage().deleteAllCookies();
        await browser.executeScript('localStorage.clear();');
        await browser.executeScript('sessionStorage.clear();');
    }

    async function login(username, password) {
        await clearCache();
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//button[contains(text(), "Login")]'));
        await browser.findElement(By.xpath('//button[contains(text(), "Login")]')).click();
        await browser.findElement(By.xpath('//input[@placeholder="Your username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@placeholder="Your strong password"]')).sendKeys(password);
        await browser.findElement(By.id('login--dialog-button-submit')).click();
        await waitForElement(By.id('header--button-account'));
    }

    async function checkPackageVisible() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(4000);
        await waitForElement(By.xpath('//span[contains(text(), "verdaccio-test")]'));
    }

    async function checkNoPackages() {
        await clearCache();
        await browser.get('https://' + app.fqdn);
        await browser.sleep(4000);
        await waitForElement(By.xpath('//*[contains(text(), "No Package Published Yet")]'));
    }

    async function npmLogin(u, p, e) {
        const cmd = `(echo "${u}"; sleep 3; echo "${p}") | npm login --auth-type=legacy --registry https://${app.fqdn}/`;
        console.log(cmd);
        await execAsync(cmd, EXEC_ARGS);
    }

    async function publishPackage(u, p, e) {
        await timers.setTimeout(10000);

        const config = {
            ssl: {
                strict: false
            }
        };

        const client = new RegClient(config);

        const params = {
            timeout: 5000,
            auth: {
                username: u,
                password: p,
                email: e,
                alwaysAuth: true
            },
            metadata: require('./package.json'),
            body: fs.createReadStream('./package.json') // not really a tarball, but doesn't matter
        };

        await new Promise((resolve, reject) => {
            client.publish(`https://${app.fqdn}/npm`, params, function (error, data, raw /* , res */) {
                console.log(error);
                console.log(data);
                console.log(raw);
                if (error) reject(error); else resolve();
            });
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('does not allow external users', async function () {
        const [error] = await safe(npmLogin('test', 'test', 'test@test.com'));
        if (!error) throw new Error('Expecting login error');
        expect(error.stderr.toString()).to.contain('bad username/password, access denied');
    });

    it('can login using npm', async function () { await npmLogin(username, password, email); });
    it('can publish package', async function () { await publishPackage(username, password, email); });

    it('cannot view package on website when not logged in', checkNoPackages);
    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(10000);
    });

    it('cannot view package on website when not logged in', checkNoPackages);
    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(10000);
    });

    it('cannot view package on website when not logged in', checkNoPackages);
    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('move to different location', async function () {
        browser.manage().deleteAllCookies();

        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(10000);

        getAppInfo();
    });

    it('cannot view package on website when not logged in', checkNoPackages);
    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id org.eggertsson.verdaccio --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login using npm', async function () { await npmLogin(username, password, email); });
    it('can publish package', async function () { await publishPackage(username, password, email); });

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login.bind(null, username, password));
    it('can view package on website', checkPackageVisible);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });
});
