# Verdaccio Cloudron App

This repository contains the Cloudron app package source for [Verdaccio](https://github.com/verdaccio/verdaccio).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.eggertsson.verdaccio)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.eggertsson.verdaccio
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd verdaccio-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok. The tests expect port 29418 to be available.

```
cd verdaccio-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> EMAIL=<cloudron email> mocha --bail test.js
```

