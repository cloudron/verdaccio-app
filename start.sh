#!/bin/bash

set -eu

if [[ ! -f /app/data/config.yaml ]]; then
    echo "==> Copying config on first run"
    cp /app/pkg/config.yaml.template /app/data/config.yaml
fi

yq eval -i ".auth.ldap.client_options.url=\"${CLOUDRON_LDAP_URL}\"" /app/data/config.yaml
yq eval -i ".auth.ldap.client_options.bindDn=\"${CLOUDRON_LDAP_BIND_DN}\"" /app/data/config.yaml
yq eval -i ".auth.ldap.client_options.bindCredentials=\"${CLOUDRON_LDAP_BIND_PASSWORD}\"" /app/data/config.yaml
yq eval -i ".auth.ldap.client_options.searchBase=\"${CLOUDRON_LDAP_USERS_BASE_DN}\"" /app/data/config.yaml

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "==> Starting Verdaccio"
# verdaccio is installed globally /usr/local/node-10.15.3/bin/verdaccio
export VERDACCIO_FORWARDED_PROTO=https
export VERDACCIO_PUBLIC_URL="${CLOUDRON_APP_ORIGIN}"
exec /usr/local/bin/gosu cloudron:cloudron verdaccio --config /app/data/config.yaml
